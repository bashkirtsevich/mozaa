unit round_Xsig_unit;

interface

uses fpu_types;

{$include fpu_const.pas}

function round_Xsig (x: PXsig): intg;
function norm_Xsig (x: PXsig): intg;

implementation

function round_Xsig (x: PXsig): intg;
  var
    n: intg;
  begin
    n := 0;

    if (x^.msw = 0) then
      begin
      x^.msw := x^.midw;
      x^.midw := x^.lsw;
      x^.lsw := 0;
      n := 32;
      end;
    while ((x^.msw and $8000) = 0) do
      begin
      x^.msw := x^.msw shl 1;
      if (x^.midw and $8000) <> 0 then
        x^.msw := x^.msw or 1;
      x^.midw := x^.midw shl 1;
      if (x^.lsw and $8000) <> 0 then
        x^.midw := x^.midw or 1;
      x^.lsw := x^.lsw shl 1;
      Inc(n);
      end;
    if (x^.lsw and $8000) <> 0 then
      begin
      Inc(x^.midw);
      if (x^.midw = 0) then
        Inc(x^.msw);
      if (x^.msw = 0) then
        begin
        x^.msw := $8000;
        Dec(n);
        end;
      end;
    Result := -n;
  end;


function norm_Xsig (x: PXsig): intg;
  var
    n: intg;
  begin
    n := 0;

    if (x^.msw = 0) then
      begin
      if (x^.midw = 0) then
        begin
        x^.msw := x^.lsw;
        x^.midw := 0;
        x^.lsw := 0;
        n := 64;
        end
      else
        begin
        x^.msw := x^.midw;
        x^.midw := x^.lsw;
        x^.lsw := 0;
        n := 32;
        end;
      end;
    while ((x^.msw and $8000) = 0) do
      begin
      x^.msw := x^.msw shl 1;
      if (x^.midw and $8000) <> 0 then
        x^.msw := x^.msw or 1;
      x^.midw := x^.midw shl 1;
      if (x^.lsw and $8000) <> 0 then
        x^.midw := x^.midw or 1;
      x^.lsw := x^.lsw shl 1;
      Inc(n);
      end;

    Result := -n;
  end;

end.
