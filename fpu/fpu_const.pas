const SW_Backward    	= $8000;	{ backward compatibility }
const SW_C3		        = $4000;	{ condition bit 3 }
const SW_Top	      	= $3800;	{ top of stack }
const SW_Top_Shift  	= 11;	{ shift for top of stack bits }
const SW_C2		        = $0400;	{ condition bit 2 }
const SW_C1	        	= $0200;	{ condition bit 1 }
const SW_C0	        	= $0100;	{ condition bit 0 }
const SW_Summary     	= $0080;	{ exception summary }
const SW_Stack_Fault	= $0040;	{ stack fault }
const SW_Precision   	= $0020;	{ loss of precision }
const SW_Underflow   	= $0010;	{ underflow }
const SW_Overflow    	= $0008;	{ overflow }
const SW_Zero_Div    	= $0004;	{ divide by zero }
const SW_Denorm_Op   	= $0002;	{ denormalized operand }
const SW_Invalid     	= $0001;	{ invalid operation }

const EXP_BIAS	=0;
const EXP_OVER	=$4000;    { smallest invalid large exponent }
const	EXP_UNDER	=-$3fff;   { largest invalid small exponent }
const EXP_WAY_UNDER   =-$6000;   { Below the smallest denormal, but still a 16 bit nr. }
const EXP_Infinity    = EXP_OVER;
const EXP_NaN         = EXP_OVER;

const EXTENDED_Ebias =$3fff;
const EXTENDED_Emin =-$3ffe;  { smallest valid exponent }

const SIGN_POS	=0;
const SIGN_NEG	=$80;

const SIGN_Positive	=0;
const SIGN_Negative	=$8000;


{ Keep the order TAG_Valid, TAG_Zero, TW_Denormal }
{ The following fold to 2 (Special; in the Tag Word }
const TW_Denormal     =4;        { De-normal }
const TW_Infinity	=5;	{ + or - infinity }
const	TW_NaN		=6;	{ Not a Number }
const	TW_Unsupported	=7;	{ Not supported by an 80486 }

const TAG_Valid	=0;	{ valid }
const TAG_Zero	=1;	{ zero }
const TAG_Special	=2;	{ De-normal, + or - infinity,
					   or Not a Number }
const TAG_Empty	=3;	{ empty }

const LOADED_DATA	=10101;	{ Special st(; number to identify
					   loaded data (not on stack;. }

{ A few flags (must be >= $10;. }
const REV            = $10;
const DEST_RM        = $20;
const LOADED         = $40;

const FPU_Exception_c   =$8000;   { Added to tag returns. }

const CW_RC		= $0C00;	{ rounding control }
const CW_PC		= $0300;	{ precision control }

const CW_Precision	= $0020;	{ loss of precision mask }
const CW_Underflow	= $0010;	{ underflow mask }
const CW_Overflow	= $0008;	{ overflow mask }
const CW_ZeroDiv	= $0004;	{ divide by zero mask }
const CW_Denormal	= $0002;	{ denormalized operand mask }
const CW_Invalid	= $0001;	{ invalid operation mask }

const CW_Exceptions  	= $003f;	 // all masks

const RC_RND		= $0000;
const RC_DOWN		= $0400;
const RC_UP		= $0800;
const RC_CHOP		= $0C00;

{ p 15-5: Precision control bits affect only the following:
   ADD, SUB(R;, MUL, DIV(R;, and SQRT }
const PR_24_BITS        = $000;
const PR_53_BITS        = $200;
const PR_64_BITS        = $300;
const PR_RESERVED_BITS  = $100;
{ FULL_PRECISION simulates all exceptions masked }
const FULL_PRECISION = (PR_64_BITS or RC_RND or $3f);

const SW_Exc_Mask     = $27f;  { Status word exception bit mask }
const	EX_INTERNAL	= $8000;	// Internal error in wm-FPU-emu 

const FPU_BUSY        =$8000;   { FPU busy bit (8087 compatibility; }
const EX_ErrorSummary =$0080;   { Error summary status }
{ Special exceptions: }
const EX_StackOver	=$0041 or SW_C1;	{ stack overflow }
const EX_StackUnder	=$0041;	{ stack underflow }
{ Exception flags: }
const EX_Precision	=$0020;	{ loss of precision }
const EX_Underflow	=$0010;	{ underflow }
const EX_Overflow	=$0008;	{ overflow }
const EX_ZeroDiv	=$0004;	{ divide by zero }
const EX_Denormal	=$0002;	{ denormalized operand }
const EX_Invalid	=$0001;	{ invalid operation }


const PRECISION_LOST_UP   = (EX_Precision or SW_C1);
const PRECISION_LOST_DOWN = EX_Precision;


const PROTECTED_ = 4;


const VERIFY_READ = 0;

const VERIFY_WRITE = 1;


const COMP_A_gt_B	= 1;

const COMP_A_eq_B	= 2;
const COMP_A_lt_B	= 3;
const COMP_No_Comp	= 4;
const COMP_Denormal	= $20;
const COMP_NaN 	=	$40;
const COMP_SNaN	 = $80;



