{$ifdef LINUX}
{$include ../defines.pas}
{$else}
{$include ..\defines.pas}
{$endif}
unit fpu_types;

interface

uses config;

type
  pu8     = ^u8;
  u8      = Bit8u;
  s8      = Bit8s;
  pu16    = ^u16;
  u16     = Bit16u;
  ps16    = ^s16;
  s16     = Bit32s;
  pu32    = ^u32;
  u32     = Bit32u;
  ps32    = ^s32;
  s32     = Bit32s;
  pu64    = ^u64;
  u64     = Bit64u;
  ps64    = ^s64;
  s64     = Bit64s;
  pu_char = ^u_char;
  u_char  = byte;
  u_short = byte;
  u_int   = word;
  u_long  = longword;
  intg    = integer;
  long    = longint;

  pfloat = ^float;
  float  = single;

  bx_ptr_equiv_t = Bit32u;

  pinfo = ^info;

  info = record
    donotindexme: byte;
  end;

  overrides = record
    address_size, operand_size, segment: u_char;
  end;

  fpu_addr_modes = record
    override_:    overrides;
    default_mode: u_char;
  end;

  paddress = ^address;

  address = record
    offset:   u32;
{$ifdef EMU_BIG_ENDIAN}
  empty:u32;
  opcode:u32;
  selector:u32;
{$else}
    selector: u32;
    opcode:   u32;
    empty:    u32;
{$endif}
  end;

  Pfpu__reg = ^fpu__reg;

  fpu__reg = record
{$ifdef EMU_BIG_ENDIAN}
  sigh:u32;
  sigl:u32;
  exp:s16;   { Signed quantity used in internal arithmetic. }
{$else}
    sigl: u32;
    sigh: u32;
    exp:  s16;   { Signed quantity used in internal arithmetic. }
{$endif}
  end;
  PFPU_REG = ^FPU_REG;
  FPU_REG  = fpu__reg;

  parray64    = ^array64;
  array64     = array[0..16] of u64;
  parray64_10 = ^array64_10;
  array64_10  = array[0..10] of u64;
  array_64_1  = array[0..1] of u64;

  parr64 = ^arr64;
  arr64  = array of u64;

  i387_t = record
    soft: record
      cwd:       s32;
      swd:       s32;
      twd:       s32;
      fip:       s32;
      fcs:       s32;
      foo:       s32;
      fos:       s32;
      fill0:     u32;        // to make sure the following aligns on an 8byte boundary
      st_space:  array64;    // 8*16 bytes per FP-reg (aligned) = 128 bytes
      ftop:      byte;
      no_update: byte;
      rm:        byte;
      alimit:    byte;
    end;
  end;


  PXsig = ^Xsig;

  Xsig = record

    lsw:  u32;
    midw: u32;
    msw:  u32;
  end;


  FUNC_ST0 = procedure (st0_ptr: PFPU_REG; st0tag: u_char);

procedure FPU_copy_to_reg1 (const r: PFPU_REG; tag: u_char);

implementation

uses fpu_system, Service_fpu, errors;

procedure FPU_copy_to_reg1 (const r: PFPU_REG; tag: u_char);
  begin
    reg_copy(r, st(1));
    FPU_settagi(1, tag);
  end;

end.
