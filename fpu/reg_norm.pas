unit reg_norm;

interface

uses fpu_types;

{$include fpu_const.pas}
function FPU_normalize_nuo (x: PFPU_REG; bias: intg): intg;

implementation

function FPU_normalize_nuo (x: PFPU_REG; bias: intg): intg;
  begin
    if ((x^.sigh and $8000) = 0) then
      begin
      if (x^.sigh = 0) then
        begin
        if (x^.sigl = 0) then
          begin
          x^.exp := EXP_UNDER;
          Result := TAG_Zero;
          exit;
          end;
        x^.sigh := x^.sigl;
        x^.sigl := 0;
        x^.exp  := x^.exp - 32;
        end;
      while ((x^.sigh and $8000) = 0) do
        begin
        x^.sigh := x^.sigh shl 1;
        if (x^.sigl and $8000) <> 0 then
          x^.sigh := x^.sigh or 1;
        x^.sigl := x^.sigl shl 1;
        Dec(x^.exp);
        end;
      end;

    x^.exp := x^.exp + bias;

    Result := TAG_Valid;
  end;

end.
