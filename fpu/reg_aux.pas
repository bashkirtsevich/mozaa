unit reg_aux;

interface

procedure fnop;
procedure fclex;
procedure finit ();

{$include fpu_const.pas}

implementation

uses fpu_system, fpu_types;

procedure fnop;
  begin
  end;

procedure fclex;
  begin
    I387.soft.swd := I387.soft.swd and not (SW_Backward or SW_Summary or
      SW_Stack_Fault or SW_Precision or SW_Underflow or SW_Overflow or
      SW_Zero_Div or SW_Denorm_Op or SW_Invalid);
    I387.soft.no_update := 1;
  end;

procedure finit ();
  begin
    I387.soft.cwd  := $037f;
    I387.soft.swd  := 0;
    I387.soft.ftop := 0;            (* We don't keep top in the status word internally. *)
    I387.soft.twd  := $ffff;
  (* The behaviour is different from that detailed in
     Section 15.1.6 of the Intel manual *)
    paddress(@I387.soft.foo)^.offset := 0;
    paddress(@I387.soft.foo)^.selector := 0;
    paddress(@I387.soft.fip)^.offset := 0;
    paddress(@I387.soft.fip)^.selector := 0;
    paddress(@I387.soft.fip)^.opcode := 0;
    I387.soft.no_update := 1;
  end;


end.
