unit shr_Xsig_unit;

interface

uses fpu_types;

procedure shr_Xsig (arg: PXsig; nr: intg);

implementation

procedure shr_Xsig (arg: PXsig; nr: intg);
  var
    n: intg;
  begin
    n := nr;

    while (n >= 32) do
      begin
      arg^.lsw := arg^.midw;
      arg^.midw := arg^.msw;
      arg^.msw := 0;
      n := n - 32;
      end;

    if (n <= 0) then
      exit;

    arg^.lsw  := (arg^.lsw shr n) or (arg^.midw shl (32 - n));
    arg^.midw := (arg^.midw shr n) or (arg^.msw shl (32 - n));
    arg^.msw  := arg^.msw shr n;

  end;

end.
