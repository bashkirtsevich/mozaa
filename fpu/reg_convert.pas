unit reg_convert;

interface

uses fpu_types, service_fpu;

function FPU_to_exp16 (a: PFPU_REG; x: PFPU_REG): intg;

implementation

uses reg_norm, errors;

function FPU_to_exp16 (a: PFPU_REG; x: PFPU_REG): intg;
  var
    sign: intg;
  begin
    sign := getsign(a);

{$ifndef EMU_BIG_ENDIAN}
    ps64(@x^.sigl)^ := ps64(@a^.sigl)^;
{$else}
  *(s64 *)@(x^.sigh) := *(const s64 *)@(a^.sigh);
{$endif}

    (* Set up the exponent as a 16 bit quantity. *)
    setexponent16(x, exponent(a));

    if (exponent16(x) = EXP_UNDER) then
      begin
      (* The number is a de-normal or pseudodenormal. *)
      (* We only deal with the significand and exponent. *)

      if (x^.sigh and $80000000) <> 0 then
        begin
        (* Is a pseudodenormal. *)
    (* This is non-80486 behaviour because the number
       loses its 'denormal' identity. *)
        addexponent(x, 1);
        end
      else
        begin
        (* Is a denormal. *)
        addexponent(x, 1);
        FPU_normalize_nuo(x, 0);
        end;
      end;

    if ((x^.sigh and $80000000) = 0) then
      begin
      fpu_EXCEPTION(EX_INTERNAL or $180);
      end;

    Result := sign;
  end;


end.
